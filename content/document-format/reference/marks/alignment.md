---
title: Mark - alignment
platform: none
product: document-format
category: reference
subcategory: marks
date: "2019-07-01"
---

# Mark - alignment

## Purpose

The `alignment` mark is intended to be applied to paragraph and headings to align these nodes to the end (right) or center.

## Example

```json
 {
  "type": "paragraph",
  "content": [
    {
      "type": "text",
      "text": "Hello world"
    }
  ],
  "marks": [
    {
      "type": "alignment",
      "attrs": {
        "align": "center"
      }
    }
  ]
}
```

## Fields

| Name       | Mandatory | Type   | Value                  |
| ---------- | --------- | ------ | ---------------------- |
| type       | ✔         | string | "alignment"             |
| attrs      | ✔         | object |                        |
| attrs.align | ✔         | string | "center" or "end" |
