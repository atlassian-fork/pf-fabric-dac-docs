---
title: Mark - link
platform: none
product: document-format
category: reference
subcategory: marks
date: "2018-03-16"
---

# Mark - link

## Purpose

The `link` is intended to be applied to [text](/platform/document-format/reference/nodes/text) nodes to annotate the text with a hyperlink.

## Example

```json
{
  "type": "text",
  "text": "Hello world",
  "marks": [
    {
      "type": "link",
      "attrs": {
        "href": "http://google.com",
        "title": "Google"
      }
    }
  ]
}
```

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"link"||
|attrs|✔|object|||
|attrs.collection||string|||
|attrs.href|✔|string|URI|The `href` value for a HTML &lt;a&gt; element|
|attrs.id||string|||
|attrs.occurrenceKey||string|||
|attrs.title||string|Title for the URI|The `title` value for a HTML &lt;a&gt; element. Generally displayed on hover|
