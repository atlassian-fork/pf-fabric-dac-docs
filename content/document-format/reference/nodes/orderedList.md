---
title: Node - orderedList
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-03-08"
---

# Node - orderedList

## Purpose

The `orderedList` node is a container for [listItem](/platform/document-format/reference/nodes/listItem) nodes.

It's the ordered equivalent of [bulletList](/platform/document-format/reference/nodes/bulletList).

## Example

```json
{
  "type": "orderedList",
  "attrs": {
    "order": 3
  },
  "content": [
    {
      "type": "listItem",
      "content": [
        {
          "type": "paragraph",
          "content": [
            {
              "type": "text",
              "text": "Hello world"
            }
          ]
        }
      ]
    }
  ]
}
```

## Content

`orderedList` is a block node in the [top-level blocks](/platform/document-format#top-level-blocks-group-nodes) group that contains [listItem](/platform/document-format/reference/nodes/listItem) nodes as children.

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"orderedList"||
|content|✔|array|Array of [listItem](/platform/document-format/reference/nodes/listItem) nodes||
|attrs||object|||
|attrs.order||integer|First list item number|e.g. 3 would mean the list starts at number three. When not specified, the list starts from 1|
