---
title: Node - decisionItem
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-11-09"
---

# Node - decisionItem

## Purpose

The `decisionItem` node represents a decision in a [decisionList](/platform/document-format/reference/nodes/decisionList), and contains the content for that decision.

## Example

```json
{
  "type": "decisionItem",
  "attrs": {
    "localId": "item-test-id",
    "state": "DECIDED"
  },
  "content": [
    {
      "type": "text",
      "text": "Start using Document Format for apps"
    }
  ]
}
```

## Content

`decisionItem` can only contain [inline](/platform/document-format#inlines-group-nodes) nodes.

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"decisionItem"||
|content||array|Array of one or more **inline** nodes||
|attrs|✔|object|||
|attrs.localId|✔|string|UUID|This should be a unique identifier per document|
|attrs.state|✔|string|"UNDECIDED" \| "DECIDED"||
