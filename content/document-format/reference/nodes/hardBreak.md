---
title: Node - hardBreak
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-03-08"
---

# Node - hardBreak

## Purpose

The `hardBreak` node is an inline node that represents a new line in a text string.

It's essentially the equivalent to a &lt;br/&gt; in HTML.

## Example

```json
{
  "version": 1,
  "type": "doc",
  "content": [
    {
      "type": "paragraph",
      "content": [
        {
          "type": "text",
          "text": "Hello"
        },
        {
          "type": "hardBreak"
        },
        {
          "type": "text",
          "text": "world"
        },
      ]
    }
  ]
}
```

## Content

`hardBreak` is an immutable node in the [inlines](/platform/document-format#inlines-group-nodes) group that can not be modified.

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|attrs||object|||
|attrs.text||string|"\n"||
|type|✔|string|"hardBreak"||
