---
title: Node - listItem
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-12-17"
---
# Node - listItem

## Purpose

The `listItem` node represents an item in a list (either [bulletList](/platform/document-format/reference/nodes/bulletList) or [orderedList](/platform/document-format/reference/nodes/orderedList)), and contains the content for that item.

## Example

```json
{
  "type": "listItem",
  "content": [
    {
      "type": "paragraph",
      "content": [
        {
          "type": "text",
          "text": "Hello world"
        }
      ]
    }
  ]
}
```

## Content

`listItem` is a block node in block node in [top-level blocks](/platform/document-format#top-level-blocks-group-nodes) group that contains other nodes from top-level blocks as children.

## Fields

|Name   |Mandatory|Type  |Value       |Notes|
|-------|---------|------|------------|-----|
|type|✔|string|"listItem"||
|content|✔|array|Array of one or more **top-level blocks** nodes where first node MUST be paragraph, mediaSingle or codeBlock only ||
