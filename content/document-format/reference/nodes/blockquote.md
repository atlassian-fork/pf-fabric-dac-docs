---
title: Node - blockquote
platform: platform
product: document-format
category: reference
subcategory: nodes
date: "2018-11-21"
---
# Node - blockquote

## Purpose

The `blockquote` node is a container that allows nested "quoted" content in a document.

## Example

```json
{
  "type": "blockquote",
  "content": [
    {
      "type": "paragraph",
      "content": [
        {
          "type": "text",
          "text": "Hello world"
        }
      ]
    }
  ]
}
```

## Content

`blockquote` is a block node in [top-level blocks](/platform/document-format#top-level-blocks-group-nodes) that can have other nodes from top-level blocks as children.

## Fields

|Name   |Mandatory|Type  |Value       |Notes|
|-------|---------|------|------------|-----|
|type   |✔        |string|"blockquote"|     |
|content|✔        |array |Array of one or more [paragraph](/platform/document-format/reference/nodes/paragraph) nodes||
