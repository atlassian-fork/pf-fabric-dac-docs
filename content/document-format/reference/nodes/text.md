---
title: Node - text
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-07-09"
---

# Node - text

## Purpose

The `text` node represents text.

## Example

```json
{
  "type": "text",
  "text": "Hello world"
}
```

## Content

`text` is a node in the [inlines](/platform/document-format#inlines-group-nodes) group that can only contain text.

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"text"||
|text|✔|string|||
|content|✔|string|Non-empty string text content|A text node **must** contain non-empty content. It's invalid to have an empty text node|
|marks||array|Array of marks||
