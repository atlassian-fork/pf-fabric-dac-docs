---
title: Node - nestedExpand
platform: platform
product: document-format
category: reference
subcategory: nodes
date: "2019-12-17"
---

# Node - nestedExpand

## Purpose

The `nestedExpand` node is a container that allows content to be hidden or shown, similar to an accordion or disclosure widget. 

`nestedExpand` is available to avoid infinite nesting, therefore it can **only** be placed within a [TableCell](/platform/document-format/reference/nodes/table_cell) or [TableHeader](/platform/document-format/reference/nodes/table_header), where an [Expand](/platform/document-format/reference/nodes/expand) can only be placed at the top-level or inside a [LayoutColumn](/platform/document-format/reference/nodes/layoutColumn).

## Example

```json
{
  "type": "nestedExpand",
  "attrs": {
    "title": "Hello world"
  },
  "content": [
    {
      "type": "paragraph",
      "content": [
        {
          "type": "text",
          "text": "Hello world"
        }
      ]
    }
  ]
}
```

## Content

`nestedExpand` can contain an array of one-or-more:

- [Paragraph](/platform/document-format/reference/nodes/paragraph)
- [Heading](/platform/document-format/reference/nodes/heading)
- [MediaGroup](/platform/document-format/reference/nodes/mediaGroup)
- [MediaSingle](/platform/document-format/reference/nodes/mediaSingle)


## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|content|✔|Array of one-or-more above mentioned nodes.|||
|type|✔|string|"nestedExpand"||
|attrs|✔|object|||
|attrs.title||string|||
