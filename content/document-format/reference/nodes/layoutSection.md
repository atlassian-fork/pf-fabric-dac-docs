---
title: Node - layoutSection
platform: platform
product: document-format
category: reference
subcategory: nodes
date: "2019-06-12"
---

# Node - layoutSection

## Purpose

The `layoutSection` node acts as a block container for columns. 

## Content

Each [layoutColumn](/platform/document-format/reference/nodes/layoutColumn) within can have its own width. Those columns widths should sum to `100`, representing 100% width of the layoutSection.

## Marks

You can apply breakout marks to the `layoutSection`. As a container, this makes the container itself change size, but the relative widths of each child remains the same.

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|content|✔|array|Array of either 2 or 3 [layoutColumn](/platform/document-format/reference/nodes/layoutColumn) nodes||
|type|✔|string|"layoutSection"||
|marks||array|An optional "breakout" mark||