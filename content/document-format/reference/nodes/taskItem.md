---
title: Node - taskItem
platform: none
product: document-format
category: reference
subcategory: nodes
date: "2018-11-09"
---

# Node - taskItem

## Purpose

The `taskItem` node represents a task in a [taskList](/platform/document-format/reference/nodes/taskList), and contains the content for that task.

## Example

```json
{
  "type": "taskItem",
  "attrs": {
    "localId": "test-id",
    "state": "TODO"
  },
  "content": [
    {
      "type": "text",
      "text": "Do this!"
    }
  ]
}
```

## Content

`taskItem` can only contain [inline](/platform/document-format#inlines-group-nodes) nodes.

## Fields
|Name|Mandatory|Type|Value|Notes|
|---|---|---|---|---|
|type|✔|string|"taskItem"||
|content| |array|Array of one or more inline nodes||
|attrs|✔|object|||
|attrs.localId|✔|string|UUID|Should be unique within the document|
|attrs.state|✔|string|"TODO" \| "DONE"||
