---
title: Atlassian Document Format
platform: platform
product: document-format
category: devguide
subcategory: index
date: "2018-04-19"
---
# Atlassian Document Format

## Purpose

Atlassian Document Format (ADF) is used to send messages. Canonically, Atlassian Document Format is represented as a JSON object, describing a tree of nodes where leafs may have a set of marks. Every document starts with a root doc node and have a "version" field equal to 1. The simplest document in ADF:

```json
{
  "version": 1,
  "type": "doc",
  "content": []
}
```

## JSON schema

A JSON schema which validates documents to ensure they conform to ADF.
The latest version can always be found at: [http://go.atlassian.com/adf-json-schema](http://go.atlassian.com/adf-json-schema).

## Nodes

Documents are composed of nodes, in much the same way that HTML documents are. A document is "ordered", in that there's a single sequential path through it. This means that traversing a document and concatenating the nodes together will yield content in the correct order.

Nodes come in two kinds: block and inline. Block nodes are permitted to have a content property, containing an array of nodes. An example of a block node is a paragraph containing text nodes:

```json
{
  "version": 1,
  "type": "doc",
  "content": [
    {
      "type": "paragraph",
      "content": [
        {
          "type": "text",
          "text": "Some text in a paragraph"
        }
      ]
    }
  ]
}
```

## "top-level blocks" group nodes

What these nodes have in common is that they can all exist at the same level. For example where-ever a [paragraph](/platform/document-format/reference/nodes/paragraph) can occur, so too can a [blockquote](/platform/document-format/reference/nodes/blockquote).

What these nodes don't have in common is their content. For example a [bulletList](/platform/document-format/reference/nodes/bulletList) can only contain [listItem](/platform/document-format/reference/nodes/listItem), but a [rule](/platform/document-format/reference/nodes/rule) cannot contain anything.

 * [applicationCard](/platform/document-format/reference/nodes/applicationCard)
 * [blockquote](/platform/document-format/reference/nodes/blockquote)
 * [bulletList](/platform/document-format/reference/nodes/bulletList)
 * [codeBlock](/platform/document-format/reference/nodes/codeBlock)
 * [decisionList](/platform/document-format/reference/nodes/decisionList)
 * [heading](/platform/document-format/reference/nodes/heading)
 * [layoutSection](/platform/document-format/reference/nodes/layoutSection)
 * [mediaGroup](/platform/document-format/reference/nodes/mediaGroup)
 * [orderedList](/platform/document-format/reference/nodes/orderedList)
 * [panel](/platform/document-format/reference/nodes/panel)
 * [paragraph](/platform/document-format/reference/nodes/paragraph)
 * [rule](/platform/document-format/reference/nodes/rule)
 * [taskList](/platform/document-format/reference/nodes/taskList)
 * [expand](/platform/document-format/reference/nodes/expand)

## "inlines" group nodes

These nodes can exist at the same level.

 * [emoji](/platform/document-format/reference/nodes/emoji)
 * [hardBreak](/platform/document-format/reference/nodes/hardBreak)
 * [mention](/platform/document-format/reference/nodes/mention)
 * [status](/platform/document-format/reference/nodes/status)
 * [text](/platform/document-format/reference/nodes/text)

## No group

 * [doc](/platform/document-format/reference/nodes/doc)
 * [layoutColumn](/platform/document-format/reference/nodes/layoutColumn)
 * [listItem](/platform/document-format/reference/nodes/listItem) (block)
 * [media](/platform/document-format/reference/nodes/media)
 * [decisionItem](/platform/document-format/reference/nodes/decisionItem)
 * [taskItem](/platform/document-format/reference/nodes/taskItem)
 * [nestedExpand](/platform/document-format/reference/nodes/nestedExpand)

# Marks

A mark can annotate inline nodes. It has `type` and optionally an `attrs` describing attributes.
Marks are applied to inline nodes (via the marks property). Different nodes support different sets of marks (e.g. `emoji` does not support any marks, but `text` supports many).

```json
{
  "version": 1,
  "type": "doc",
  "content": [
    {
      "type": "paragraph",
      "content": [
        {
          "type": "text",
          "marks": [
            {
              "type": "strong"
            }
          ],
          "text": "Some strong text"
        }
      ]
    }
  ]
}
```

## Type

Just like nodes, marks have a "type". A strong mark:

```json
{
  "type": "strong"
}
```

## Attributes

Just like nodes, marks can have attributes via the attrs property. A link mark with a href attribute:

```json
{
  "type": "link",
  "attrs": {
    "href": "http://google.com"
  }
}
```
